-- Valentina Studio --
-- MySQL dump --
-- ---------------------------------------------------------


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
-- ---------------------------------------------------------


-- CREATE DATABASE "itsm_music_bot" ------------------------
CREATE DATABASE IF NOT EXISTS `itsm_music_bot` CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `itsm_music_bot`;
-- ---------------------------------------------------------


-- CREATE TABLE "song" -------------------------------------
CREATE TABLE `song` ( 
	`id` Int( 11 ) NOT NULL AUTO_INCREMENT,
	`uid` Int( 11 ) NOT NULL,
	`song` VarChar( 255 ) NOT NULL,
	PRIMARY KEY ( `id` ) )
ENGINE = InnoDB;
-- ---------------------------------------------------------


-- CREATE TABLE "karma" ------------------------------------
CREATE TABLE `karma` ( 
	`uid` Int( 11 ) NOT NULL,
	`karma_rate` Int( 11 ) NOT NULL,
	`username` VarChar( 255 ) NOT NULL,
	PRIMARY KEY ( `uid` ) )
ENGINE = InnoDB;
-- ---------------------------------------------------------


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
-- ---------------------------------------------------------


